package org.rajesh.examples.models;

/**
 * Created by Rajesh on 8/7/2014.
 */
public class Employee {
    private String name;
    private String qualification;
    private int age;

    public Employee(String name, String qualification, int age) {
        this.name = name;
        this.qualification = qualification;
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getQualification() {
        return qualification;
    }

    public void setQualification(String qualification) {
        this.qualification = qualification;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    @Override
    public String toString() {
        return "Employee{" +
                "name='" + name + '\'' +
                ", qualification='" + qualification + '\'' +
                ", age=" + age +
                '}';
    }
}
