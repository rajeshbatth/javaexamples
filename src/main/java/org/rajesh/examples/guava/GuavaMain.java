package org.rajesh.examples.guava;

/**
 * Created by Rajesh on 8/7/2014.
 */
public class GuavaMain {
    public static void main(String[] args) {
//        validations();
        collections();
    }

    private static void collections(){
        Collections collections = new Collections();
        collections.predicateExample();
    }

    private static void validations() {
        Validations validations = new Validations(null, "Mountain vs Viper", 1);
        System.out.printf("s1: " + validations.getString1());
        try {
            validations.addToStrings(null);
            assert true;
        } catch (IllegalStateException e) {
            System.err.println(e);
        }

        try {
            validations.addToStrings("Watchers of the wall");
            assert true;
        } catch (IllegalStateException e) {
            System.err.println(e);
        }

        validations.addToStrings("GOT");
        System.out.println(validations.getStrings());
    }
}
