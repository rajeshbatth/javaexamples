package org.rajesh.examples.guava;

import com.google.common.base.Preconditions;
import com.google.common.base.Strings;
import com.google.common.collect.Constraint;
import com.google.common.collect.Constraints;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * Created by Rajesh on 8/7/2014.
 */
public class Validations {
    private String string1;
    private String string2;
    private int i;

    private List<String> strings = Constraints.constrainedList(new ArrayList<String>(), new Constraint<String>() {
        @Override
        public String checkElement(String s) {
            Preconditions.checkState(!Strings.isNullOrEmpty(s), "Empty item");
            Preconditions.checkState(!(s.length() > 3), "Max length is 3");
            return s;
        }
    });

    public Validations(String s1, String s2, int i) {
        string1 = Optional.ofNullable(s1).orElse("Default Value");
        string2 = Preconditions.checkNotNull(s2, "Second argument is null");
        Preconditions.checkState(i > 0, "i should be greater than zero");
    }

    public String getString1() {
        return string1;
    }

    public void setString1(String string1) {
        this.string1 = string1;
    }

    public String getString2() {
        return string2;
    }

    public void setString2(String string2) {
        this.string2 = string2;
    }

    public int getI() {
        return i;
    }

    public void setI(int i) {
        this.i = i;
    }

    public void addToStrings(String s) {
        strings.add(s);
    }

    public List<String> getStrings() {
        return strings;
    }
}
