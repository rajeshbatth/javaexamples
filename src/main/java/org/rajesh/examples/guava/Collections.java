package org.rajesh.examples.guava;

import com.google.common.base.Function;
import com.google.common.base.Predicate;
import com.google.common.collect.*;
import org.rajesh.examples.models.Department;
import org.rajesh.examples.models.Employee;

import javax.annotation.Nullable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Created by Rajesh on 8/7/2014.
 */
public class Collections {
    private final List<Employee> employees = new ArrayList<Employee>();

    public Collections() {
        for (int i = 1; i <= 10; i++) {
            employees.add(new Employee("Employee" + i, "B.E", 25 + (i * 3)));
        }
    }

    public void multiMapExample() {
        Multimap<Department, Employee> multimap = ArrayListMultimap.create();
        Department tathastu = new Department("Tathastu");
        multimap.put(tathastu, new Employee("Rajesh", "B.E.", 24));
        multimap.put(tathastu, new Employee("Srikara", "B.E.", 23));

        Department buzz = new Department("Buzz");
        multimap.put(buzz, new Employee("Prakash", "B.E.", 24));
        multimap.put(buzz, new Employee("Nag Kumar", "B.E.", 23));

        System.out.println(tathastu.getName() + " : " + multimap.get(tathastu));
        System.out.println(buzz.getName() + " : " + multimap.get(buzz));

    }

    public void biMapExample() {
        BiMap<String, String> biMap = HashBiMap.create();
        biMap.put("Hello", "Namaskara");
        biMap.put("World", "Loka");

        System.out.println(biMap.get("Hello") + " " + biMap.inverse().get("Loka"));
    }

    public void predicateExample() {
        Predicate<Employee> predicate = new Predicate<Employee>() {
            @Override
            public boolean apply(@Nullable Employee employee) {
                return employee.getAge() > 45;
            }
        };
        Function<Employee, String> function = new Function<Employee, String>() {
            @Nullable
            @Override
            public String apply(@Nullable Employee employee) {
                return employee.getName();
            }
        };

        Collection<Employee> seniorEmployees = Collections2.filter(employees, predicate);
        System.out.println(Collections2.transform(seniorEmployees,function));
    }

}
